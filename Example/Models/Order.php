<?php

namespace Models{

    class Order{
         private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }
        public function find($id)
        {
            return $this->connection->runQuery('SELECT * FROM orders WHERE id = $1', [$id])[0];
        }

        public function create($car_id, $date, $status)
        {
            $this->connection->runStatement('INSERT INTO orders (car_id, date, status) VALUES ($1, $2, $3)', [$car_id, $date, $status]);
        }

        public function delete($id)
        {
            $this->connection->runStatement('DELETE FROM orders WHERE id = $1', [$id]);
        }

        public function update($id, $car_id, $date, $status)
        {
            $this->connection->runStatement('UPDATE orders SET car_id = $2, date = $3, status = $4 WHERE id = $1', [$id, $car_id, $date, $status]);
        }

        public function all()
        {
            return $this->connection->runQuery('SELECT * FROM orders');
        }
       
    }
}
