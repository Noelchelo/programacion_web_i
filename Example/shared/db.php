<?php

require_once __DIR__ . '/../Db/PgConnection.php';
require_once __DIR__ . '/../Models/User.php';
require_once __DIR__ . '/../Models/Car.php';
require_once __DIR__ . '/../Models/Order.php';

use Db\PgConnection;

$con = new PgConnection('postgres', 'postgres', 'universidad', 5433, 'localhost');
$con->connect();

$user_model = new Models\User($con);
$car_model = new Models\Car($con);
$order_model = new Models\Order($con);

