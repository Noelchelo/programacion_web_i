<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitae2ef02a3940051f88c170d007834ece
{
    public static $classMap = array (
        'Hoy' => __DIR__ . '/../..' . '/app/models/hoy.php',
        'Martes' => __DIR__ . '/../..' . '/app/lunes/martes.php',
        'User' => __DIR__ . '/../..' . '/app/models/user.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInitae2ef02a3940051f88c170d007834ece::$classMap;

        }, null, ClassLoader::class);
    }
}
