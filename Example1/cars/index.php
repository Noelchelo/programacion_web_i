<?php
$title = 'Citas';

require_once '../shared/header.php';
require_once '../shared/db.php';
$fecha = $_POST['fecha'] ?? '';
$fecha2 = $_POST['fecha2'] ?? '';
$doctor = $_POST['doctor'] ?? '';
$paciente = $_POST['paciente'] ?? '';
?>

<div class="container">
    <h1>Citas</h1>

    <form method="POST">
        <label>Fecha</label>
        <input type="date" name="fecha">
        <label>Fecha 2</label>
        <input type="date" name="fecha2">
        <label>Doctor</label>
        <input type="text" name="doctor">
        <label>Paciente</label>
        <input type="text" name="paciente">
        <button>Buscar</button>
        </form>

    <table class="table table-striped table-bordered">
        <tr>
            <tr>
            <th>Id</th>
            <th>Fecha</th>
            <th>Doctor</th>
            <th>Paciente</th>
            <th>Motivo</th>
        </tr>
        </tr>
        <?php
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $cars = $car_model ->doc($doctor, $paciente, $fecha);
                foreach ($cars as $car) {
                    require './row.php';
            }
            
        }
        ?>
    </table>
</div>

<?php require_once '../shared/footer.php' ?>