<?php
$title = 'New Orders';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/guard.php';


$car_id = $_POST['car_id'] ?? '';
$date = $_POST['date'] ?? '';
$status = $_POST['status'] ?? '';
$orders = ['car_id' => $car_id, 'date' => $date, 'status' => $status];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once '../shared/db.php';
    $orders_model->create($car_id, $date, $status);
    return header('Location: /orders');
}
?>

<div class="container">
    <h1><?=$title?></h1>
    <?php require_once './form.php' ?>
</div>

<?php require_once '../shared/footer.php' ?>