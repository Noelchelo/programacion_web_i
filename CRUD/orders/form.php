<form method="POST">
    <div class="form-group">
        <label>Car Id: </label>
        <input type="text" name="car_id" class="form-control" required value="<?=$orders['car_id']?>">
    </div>
    <div class="form-group">
        <label>Date: </label>
        <input type="date" name="date" class="form-control" required value="<?=$orders['date']?>">
    </div>
    <div class="form-group">
        <label>Status: </label>
        <input type="text" name="status" class="form-control" required value="<?=$orders['status']?>">
    </div>
    <input class="btn btn-primary" type="submit" value="Aceptar">
    <a href="/orders" class="btn btn-danger">Cancelar</a>
</form>