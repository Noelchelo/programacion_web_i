<?php
$title = 'Edit Orders';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
require_once '../shared/guard.php';

$id    = $_GET['id'] ?? 0;
$car_id    = $_POST['car_id'] ?? 0;
$date = $_POST['date'] ?? '';
$status  = $_POST['status'] ?? '';

$orders = $orders_model->find($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $orders_model->update($id, $car_id, $date, $status);
    return header('Location: /orders');
}
?>

<div class="container">
    <h1><?=$title?></h1>

    <?php require_once './form.php' ?>
</div>

<?php require_once '../shared/footer.php' ?>