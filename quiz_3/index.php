<?php

$fecha = $_POST['fecha'] ?? '';
$fecha2 = $_POST['fecha2'] ?? '';
$doctor = $_POST['doctor'] ?? '';
$paciente = $_POST['paciente'] ?? '';
    function runQuery($sql, $params = [])
        {
            return pg_fetch_all($this->runStatement($sql, $params));
        }

    function runStatement($sql, $params = [])
        {
            return pg_query_params($this->connection, $sql, $params);
        }
    function disconnect()
        {
            pg_close($this->connection);
        }


    function all(){
        $connection = pg_connect("host=localhost port=5433 dbname=hospital user=postgres password=postgres");
         return $this->connection->runQuery('SELECT * FROM citas'); }

?>

<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <title>Citas</title>
    </head>
    <body>
        <form method="POST">
        <label>Fecha</label>
        <input type="date" name="fecha">
        <label>Fecha 2</label>
        <input type="date" name="fecha2">
        <label>Doctor</label>
        <input type="text" name="doctor">
        <label>Paciente</label>
        <input type="text" name="paciente">
        <button>Buscar</button>
        </form>

    <div class="container">
    <h1>Citas</h1>
    <table class="table table-striped table-bordered">
        <tr>
            <th>Id</th>
            <th>Fecha 1</th>
            <th>Fecha 2</th>
            <th>Doctor</th>
            <th>Paciente</th>
            <th>Motivo</th>
        </tr>
         <?php
            $citas = $cita_model->all();
            if ($citas) {
                foreach ($citas as $cita) {
                    require './row.php';
                }
            }
        ?>
    </table>
</div>
    </body>
</html>