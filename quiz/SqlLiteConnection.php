<?php
 require_once './DbConnection.php';

 class SqlLiteConnection extends DbConnection{

    
     private $pdo;
 
    public function connect() {
        if ($this->pdo == null) {
            $this->pdo = new \PDO("sqlite:C:\sqlite\quiz.db");
        }
     $this->pdo;
    }
    public function disconnect(){
        $pdo->close();
    }
    public function runQuery($sql, $params = [])
    {
        return pg_fetch_all($this->runStatement($sql, $params));
    }

    public function runStatement($sql, $params = [])
    {
        return pg_query_params($this->connection, $sql, $params);
    }
 }